package ru.t1.shipilov.tm.api.service;

import ru.t1.shipilov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
