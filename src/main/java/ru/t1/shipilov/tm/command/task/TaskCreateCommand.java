package ru.t1.shipilov.tm.command.task;

import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private final String NAME = "task-create";

    private final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(userId, name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
